import React from 'react';
import classnames from 'classnames';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import useBaseUrl from '@docusaurus/useBaseUrl';
import styles from './styles.module.css';


const features = [
  {
    title: <>Built on a Firm Foundation</>,
    imageUrl: 'img/Foundation2.svg',
    description: (
      <>
      <code>Trustworthiness</code> and <code>justice</code> are indespensible to any worthy undertaking. Without them, nothing good can grow.
      </>
    ),
  },
  {
    title: <>Acquisition of Knowledge</>,
    imageUrl: 'img/resources.svg',
    description: (
      <>
        <code>Resources</code> are abundent, and <code>knowledge</code> must be sought assiduously! Check out some of the <a href="/faqs#tools">tools I use</a>, or explore <a href="blog">my take</a> on prevailing matters.
        {/* Knowledge must be sought assiduously, and what a time we live in! Resources are abundant, no matter the task there are things to draw on, whether that means constructing from the fundamentals or leveraging an effective and generous framework. */}
      </>
    ),
  },
  {
    title: <>Application gives Fruits</>,
    imageUrl: 'img/fruits.svg',
    description: (
      <>
        In the field of <code>action</code> knowledge is tested. Here the culmination of all that striving and growing is potentially realized. Peruse a progressing writeup of my projects, including a <a href="docs/pair1">desktop application</a>, a how a <a href="docs/cert1">single html file</a> turned in to a <a href="docs/cert3">modern webapp</a>, and some ongoing endeavors.
      </>
    ),
  },
];

function Feature({imageUrl, title, description}) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames('col col--4', styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const {siteConfig = {}} = context;
  return (
    <Layout
      title={`👋😁`}
      description="Quddús George, a fullstack Javascript Developer learning what's needed to produce fruitful results.">
      <header className={classnames('hero hero--primary', styles.heroBanner)}>
        <div className="container"> 
          <h1 className="hero__title">{siteConfig.title}</h1>
            {/* <img className="hero-img" src="../static/img/fruits.svg"></img> */}
          <p className="hero__subtitle">{siteConfig.tagline}</p>
          <div className={styles.buttons}>
            <Link
              className={classnames(
                'button button--outline button--secondary button--lg',
                styles.getStarted,
              )}
              to={useBaseUrl('docs/cert1')}>
              Show me some code
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;

