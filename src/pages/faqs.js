import React from "react";
import Layout from "@theme/Layout";
import classnames from "classnames";

let faqList = [
  {
    id: "qualities",
    question: <>What are the most important qualities for a developer?</>,
    answer: (
      <>
        <p>
          Like a tasty recipe, it is the combination of the various elements
          that ensures a coherent resulting product. There are however, those
          parts which the whole cannot go without.
        </p>
        <p>
          In this regard, <b>uprightness</b>, <b>trustworthiness</b>, and{" "}
          <b>rectitude of conduct</b> ensure that the individual's work can
          truly benefits the lives of others.
          <b> Focusing the mind on a single point</b>, the ability to{" "}
          <b>break things down</b> and <b>make meaningful abstractions</b>, and
          the power of <b>reflection</b> allow for innovations, solutions and
          discovery.
          <b> Humility</b>, <b>consultation</b>, <b>kindness</b> and{" "}
          <b>enthusiasm</b> in dealing with others produce collaborations which
          can have exponentially more powerful results.
        </p>
      </>
    ),
  },
  {
    id: "plan",
    question: <>How do you plan a project?</>,
    answer: (
      <>
        Start with some questions. For example:
        <ul>
          <li>What is the user's goal? What is the creator or owner's goal?</li>
          <li>
            What are the seperate steps? (asking this recursively gives us
            access to different levels of abstraction of the object under study)
          </li>
          <li>What tools already exist to acheive a given step?</li>
          <ul>
            <li>Which is a better solution?</li>
            <li>How long would it take to learn how to use?</li>
          </ul>
          <li>What are some necessary requirements for success?</li>
          This could be, how much would it need to make to be profitable? How
          many users would have to contribute for it to become useful?
          <li>How long do I think it would take?</li>
        </ul>
        Research and try to answer the questions. Then map it out, wireframes,
        userflows, mockups, make sure that central processes are understood well
        before going in and trying to implement. Did this raise more questions?
        Great! Investigate further. Talk it through with a listening ear. Hear
        another perspective. Now with a clarified vision we can move to
        implementation.
      </>
    ),
  },
  {
    id: "motivation",
    question: <>What motivates you to develop software?</>,
    answer: (
      <>
        I want to produce results that benefit myself and others. I want to
        excercise my talents, and I want to learn and do enough to be able to
        make a contribution to the ongoing discourse and to the field itself.
      </>
    ),
  },
  {
    id: "tools",
    question: <>What tools do you use? Why did you choose them?</>,
    answer: (
      <>
        <p>
          {" "}
          There are so many great tools! Here are a few I have been enjoying
          recently:
        </p>
        <p>
          <a href="https://vscodium.com/" target="_blank">
            Vscodium
          </a>
          : VScode is great, I prefer it over atom, largely because I am not
          trying to hack my text editor at the moment. 😆 Closed source
          telemetry, not so great. Vscodium makes gives us the great work from
          microsoft and access to the many great community plugins without the
          commitment to unknown telemetry.
        </p>
        <p>
          <a href="https://www.mozilla.org/en-US/firefox/new/" target="_blank">
            Firefox
          </a>
          : Powerful dev tools. Powerful browser. Mozilla's lofty Manifesto
          pledge aligns well with my own beliefs.
        </p>
        <p>
          <a href="https://uxdesign.cc/" target="_blank">
            The UX Collective
          </a>
          : A great collection of articles, artwork, guidelines and tools for
          the field of User Experience. They recently put together a{" "}
          <a href="https://start.uxdesign.cc/" target="_blank">
            great guide
          </a>{" "}
          full of rich resources! I think the insight gained in this field is
          treasure for developers and most any other work really.
        </p>
        <p>
          <a href="https://super-productivity.com/" target="_blank">
            superProductivity
          </a>
          : Now this is a gem! I was looking all over for a tool to time my
          work, giving me as much data with as little effort as possible. All I
          found was subscribe to this and sync that. Really, I'm just looking
          for a good timer with a long history. superProductivity is{" "}
          <b>exactly</b> what I was looking for. Input tasks with as little
          clicks as possible. Track work as off hands as possible. Output as
          much data as possible. Try it out, really.
        </p>
        <p>
          <a href="https://boostnote.io/" target="_blank">
            BoostNote
          </a>
          : This is a great markdown tool for organizing notes. It helps me make
          sure I am actually accumulating knowledge and tools and not just going
          through it, creating a record of process and discovery as I go from
          project to project.
        </p>
        <p>
          <a href="https://inkscape.org/" target="_blank">
            Inkscape
          </a>
          : I use Inkscape for turning my drawings into SVGs right now. It's a
          great tool so far as I can tell and I will be delving deeper into it
          soon.
        </p>
        <p>
          <a href="https://procreate.art/pocket" target="_blank">
            Procreate Pocket
          </a>
          : I absolutely love Procreate, I do alot of my drawings from my phone
          on Procreate pocket. Such an intuitive and smooth interface, allows me
          to get right into the drawing and stay there, despite switching
          brushers, layers, and paints.
        </p>
        <p>
          <a href="https://www.vectornator.io/" target="_blank">
            Vectornator
          </a>
          : Recently I did a good bit of work in Vectornator recently and really
          enjoyed it. The Svg's were quick to draw and web optimized.
        </p>
        <p>
          <a href="https://macsvg.org/" target="_blank">
            MacSvg
          </a>
          : Great tool for drawing and animating SVGs. Well designed and
          powerful. Free and Open source.
        </p>
      </>
    ),
  },
  {
    question: <>What do you do when you aren't coding?</>,
    answer: (
      <>
        <p>
          I have three children, raising and educating them is one of my central
          goals, most important endeavors, and greatest responsibilities. It's a
          joy. Super difficult too. Wouldn't trade it for anything.
        </p>
        <p>
          I also spend anytime outside of contributing to family life and work
          trying to establish the institute process in my neighborhood. The
          training institute seeks to raise capacity within populations to
          respond to their own exigincies. Making the world a better place for
          all of us, is going to require work and an ever growing number of
          collaborators.
        </p>
        <p>
          The time we have in this world is short and fleeting, it is then even
          more important that our time is spent concerned with the needs of
          others, and not caught up in our own selves.
        </p>
      </>
    ),
  },
];

function QA({ question, answer, id }) {
  // const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames("container")}>
      {/* {imgUrl && (
          <div className="text--center">
            <img className={styles.featureImage} src={imgUrl} alt={question} />
          </div>
        )} */}
      <h3 id={id}>{question}</h3>
      <p>{answer}</p>
    </div>
  );
}

function Faqs() {
  return (
    <Layout title="FAQS">
      <div className="container">
        <h1>Faqs 🤔</h1>
        {faqList.map((props, index) => (
          <QA key={index} {...props} />
        ))}
      </div>
    </Layout>
  );
}

export default Faqs;
