import React from 'react'
import Select from 'react-select'



export default function SelectExample({courseList}){
    
    const options = courseList.map(el=>{
        return ({value: el,
        label: el})
    })
    return(
  <Select className="select-comp" placeholder="Search For a Course" id="addCourse" options={options} />
)
}