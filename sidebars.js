module.exports = {
  someSidebar: {
    Certificate_Maker: ['cert1', 'cert2', 'cert3'],
    PairUp: ['pair1', 'pair2', 'pair3', 'pair4', 'pair5'],
    // MostMelodious: ['mel1'],
    Portfoliosaurus: ['doc1', 'doc2', 'doc3'],
    CssHouse: ['house1'],
    GMBME: ['mail1']
  },
};
