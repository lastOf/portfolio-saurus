module.exports = {
  title: "Quddús George",
  tagline: `Fullstack Javascript Developer learning what's needed to produce fruitful results.`,
  url: "https://your-docusaurus-test-site.com",
  baseUrl: "/",
  favicon: "img/fruits.ico",
  organizationName: "lastOf", // Usually your GitHub org/user name.
  projectName: "portfoliosaurus", // Usually your repo name.
  themeConfig: {
    navbar: {
      title: "Quddús George",
      logo: {
        alt: "My Site Logo",
        src: "img/fruits.svg",
      },
      links: [
        {
          to: "docs/cert1",
          activeBasePath: "docs",
          label: "Projects",
          position: "left",
        },
        { to: "blog", label: "Blog", position: "left" },
        {
          to: "faqs",
          activeBasePath: "faqs",
          label: "FAQs",
          position: "left",
        },
        // {
        //   href: 'https://github.com/lastofthefirst',
        //   label: 'GitHub',
        //   position: 'right',
        // },
        // {
        //   href: 'https://gitlab.com/lastof',
        //   label: 'GitLab',
        //   position: 'right',
        // },
      ],
    },
    footer: {
      style: "dark",
      links: [
        {
          title: "Highlights",
          items: [
            {
              label: "The Beginning",
              to: "docs/cert1",
            },
            {
              label: "PairUp",
              to: "docs/pair1",
            },
            {
              label: "FAQs",
              to: "faqs",
            },
          ],
        },
        {
          title: "About",
          items: [
            {
              label: "Resume",
              href: "https://portfoliosaurus.now.sh/img/2021_resume.pdf",
            },
            {
              label: "lastof[at]pm.me",
              href: "https://protonmail.com",
            },
            {
              label: "Stack Overflow",
              href: "https://stackoverflow.com/users/10235296/quddus-george",
            },
            {
              label: "Blog",
              to: "blog",
            },
          ],
        },
        {
          title: "Code",
          items: [
            {
              label: "GitLab",
              href: "https://gitlab.com/lastof",
            },
            {
              label: "GitHub",
              href: "https://github.com/lastofthefirst",
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Portfoliosaurus by Quddús`,
    },
  },
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          // editUrl:
          //   'https://github.com/facebook/docusaurus/edit/master/website/',
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
  themes: ["@docusaurus/theme-live-codeblock"],
};
