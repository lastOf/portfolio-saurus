---
id: start
title: The Beginning
author: Quddus George
author_title: in a retrospective moment
tags: [origin story, reflection]
---
![the first program](/img/firstProgram.gif 'Logo Title Text 1')

I remember the moment very clearly. I was studying through javascript, my first time coming into contact with data structures, functions, objects and arrays. In one of my journeys into stackoverflow I came accross the html mailto property.
<!--truncate-->
With some reflection and a surge of excitement I concluded that I could use the mailto property to make my job more efficient and save tons of time!

I worked as a digital web assistant for the Wilmette Institute, an online learning institution, doing digital paperwork. One of my main tasks was creating pdf certificates for learners and emailing them out one by one.

When I joined the team certificates were over a year behind. I had already been grouping windows, making groups of ten drafts one at a time before filling out the variable information to speed up the process and reduce the number of actions.

This was a whole new level however, using the mailto property I could paste the course name, and all of the students who completed the course into some input fields and generate all the drafts at once.

That little program reduced the number of actions per group of emails by hundreds.

When I enthusiastically shared my solution with those who worked above me they were cautious. What if something went wrong? Why not do it the 'normal' way? I didn't expect it, but it was a good chance to slow down and walk through what I was doing with an aprehensive party.

In the end, they agreed that I could proceed using the little program. I only used it a few times however. The experience had raised so many questions in my mind, these questions led me to discover Nodejs, and in a short time automate my entire job.

Now, I did end up losing the majority of the pay that I would have received if I continued my job the 'normal way'. I was payed hourly and with Nodejs scripts completing weeks of work in tens of minutes my work quickly dried up. 

My employer hasn't chosen to compensate me for the resulting increase in efficiency, accuracy and value, but they did welcome me to code for any project I am working on. And instead of spending my time becoming a copy-paste ninja I delved further and further into Nodejs with browser automation, email and pdf generation, and much more. The analysis of these tradeofs is, however, a story for another time.