---
id: thePinephone
title: thePinephone
author: Quddus George
author_title: exploring new technology
tags: [open source, mobile, justice]
---
![thePinephone](/img/pinephone.jpg)
Since I began studying software I have had more and more questions about my phone. This began with initially when I discovered that the label on a button and its functionality had no inherent connection. More questions arose when I set up a pihole for my home internet. All day long the [pihole](https://pi-hole.net/) blocked network requests from the iphones in the home, even when no one was using them.

<!--truncate -->

Garann Means' [talk](https://www.youtube.com/watch?v=14wIbWGyaKg&app=desktop) entitled "What happened to my JS phone", was a great resource for understanding the situation.

*There must be a mobile operating system out there that isn't trying to collect as much data on me as possible. There has to be a Phone that I can design, change, or adjust interface elements and native applications freely and openly.*

There are so many elements of the current situation mobile phones that are suspicious, saddening, questionable, and even alarming. Like their components being produced through slave-labor conditions. Or dark practices in interface and user experience design that foster addiction and serve to manipulate users. Or the staggering amount of data collection by vested interests, that has no choice but to contribute to the suffering experienced by deep seeted systemic societal diseases such as racism and the inequal treatment of men and women.

We can be sure, these are symptoms of the disintegration of society. The old way of doing things won't work anymore. And far from paralyzing us, knowing more about these issues should incite action.

Luckily for me, I started looking when I did and not earlier, because the arena seems to have been bleak for a while now. I found a few different options on the horizon. The [Fairphone](https://www.fairphone.com/en/) tries to address the social and environmental issues that have blighted the mobile environment thus far. The [Librem 5](https://puri.sm/products/librem-5/) Focuses on the security aspect, with easy to access hardware kill switches. The [Pinephone](https://www.pine64.org/pinephone/) strives to me and accessible hardware platform for Developers to create open source operating system's. There's a lot more to each of these projects, check them out directly to learn more.

I was able to purchase a Braveheart edition of the Pinephone which arrived earlier this year. I haven't had too much time to work on it. But it is exciting to watch all the developers work hard to get software functioning well enough to be usable on a daily basis.

As soon as the softshell case I ordered arrives I plan to jump in headfirst and begin using the Pinephone as my daily driver. I plan to do without functionality that is not yet available, and be patient with the state of the software.

