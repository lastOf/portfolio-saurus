---
id: firstRustProgram
title: My first Rust Program
author: Quddus George
author_image_orl: https://portfoliosaurus.now.sh/img/foundation.svg
author_title: Fascinated with the language
tags: [Rust, Systems Programming, learning]
---
Meet **Ferris**, the unnoficial mascot of the Rust language.
![Learning Rust](/img/rustsources.svg)

Today I began my study of Rust, a systems programming language that grabbed my attention, started by Graydon Hoare and others at Mozilla.

Here are some of my initial thoughts, favorite resources to date, and my first program! 
<!--truncate -->

## Initial Takeaways

Rust seems like a great language to learn as a first lower level language. 

It has a high emphasis on safety and performance. This manifests itself first and foremost to the developer as frequent and very picky bugs. very few mistakes get through as exceptions to errors.

The redeeming tentant of Rust's frequent errors is that *errors need to come with good documentation. Each error tries to contain a link to more detailed information. For example: "Here is what we think broke, here is why we think it broke. Here is what usually solves that type of problem.*

:::success My experience
So far, I found this to be true. Writing my first program I received some 20-30 error messages. I found the message or associated documentation to be enough to solve my problem or point me in the right direction 90% of the time. This contrasts with Nodejs, where, although writing a language I am completely familiar with the errors usually direct me to a large overarching category of bugs and the code line where my problem started, which sometimes I have to wade through the error message to find.
:::

Though Rust originated at Mozilla, it is not owned or controlled by Mozilla. Mozilla does sponsor the project, and is proactively working on replacing firefox components with those built in Rust.

Rust strives to make multithreaded code come accessable, because it of its fierce error checking you can attempt to write multithreading code and know ahead of time if you've successfully avoided errors.

The compiler is often talked of as a hugely encyclopedic mentor. Providing you with the relevant information as you make mistakes. I think this is part of what will make it a great first systems level language to delve into.
## Resources

### This video helped the most
[Rust 101](https://www.youtube.com/watch?v=FMqydRampuo) a talk given by E. Dunham. She goes over a pretty wide overview the language, its features and so on. I think it also helped that I tried to get a bit familiar with the syntax first.

* [Some videos from Mozilla that helped me to get an idea for what Rust is](https://developer.mozilla.org/en-US/docs/Mozilla/Rust)
* [Rust by Example](https://doc.rust-lang.org/stable/rust-by-example/index.html)
    * This website does a walk through with excercises, helps you test your understanding as you go along. Reminds me of freecodecamp. I plan to work through it gradually.


## Syntax

### Scope
Scope seems to work similar or the same as javascript. It is measured by opening and closing curly brackets {}.

Scope can be nested
```
{
    Everything out here exists until the last closing brace.
    {
        Everything in here only exists until the next closed brace.
    }
}
```
If a value or variable or function is in scope it is called living.

"many common mistakes in rust are due to scope" - this comment made me think that it must not be as simple as javascript scope. I guess we'll know soon enough.

### Functions

Functions are denoted with *fn*.

Example function syntax
```rust

fn functionName(argument:argumentType)->returnValueType{
    return 1;
}; 

```
Expressions all need to end with a semicolon. One rare exception is return expressions which can drop the return and the semicolon like so:

```rust

fn functionName(argument:argumentType)->returnValueType{
    1+0
};
```



### Macros

This was a concept seemingly foreign to javascript which once explained, reminded me most of higher order functions. They are common use case functions written on top of the native building blocks. 
The syntax is:
`macroname!(arguments, arguments)`
The bang or exlamation always ends the macroname. The Rust equivilent of console.log(); which is println!() is the first macro that was introduced.

in println!() arguments can be interwoven into strings in order with empty curly brackets or by denoting the index

```Rust
let langName = "rust";
println!("Hello c{}acean.", langName)
 ```
This prints Hello crustacean to the console. Crustacean seems to be the pet name often adopted by Rust programmers.


### Types

Types refer to a single value. Your type could be a primitive, array a string a bolean and so on.

Stating the type allows the compiler to save memory, to allocate the most efficient amount for your variables without cutting them short.

Statically writing your type also provides useful documentation to anyone using your code in the future.


### Traits
Traits refer to the attributes of a given type. One example of a trait is *add*, this trait describes types which are addable.

You can edit and add traits with *impl*.

### Owners

Every value only has one owner. This is a new concept to me. I have to think about it more to be able to describe it. As far as I understand right now, if you refer to the value of one variable with another the second variable has the value and if you then referr to the first value you will have an error.



## My first program


Here is a link to check out [my first program](https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=209efe8f31bee7c38a26fb4743de8c22)!

It's simple, but it makes use of a part of the language that I found to be very elegant to read and write. The match conditional statement. It is like the switch statement in Javascript, with alot less punctuation.

I also like the _ underscore for default and the ... as a range descriptor.


```rust
#![allow(unused)]
fn main() {
fn ord(number:i32)->String{
    match number {
        1 => return format!("{}st",number),
        2 => return format!("{}nd",number),
        3 => return format!("{}rd",number),
        4...20 => return format!("{}th", number),
        _ => return format!("else"),
    }
}
let first = 1;
let second = 2; 
let fourth = 4;
let eighth = 8;
let sixteenth = 16;

fn example(numb:i32){
    println!("{numb} becomes {}", {ord(numb)}, numb=numb);
}
example(first);
example(second);
example(fourth);
example(eighth);
example(sixteenth);

}

```

## Conclusion

A great first day of study. After programming in Javascript a while there is a building feeling, for me, of being able to break down and learn "anything". Anything in this case is libraries written with Javascript. While I know it's not true, it was very helpful to step into a programming language just barely, but enough to see many concepts I am wholly unfamiliar with.

The study of rust also helped me really understand alot better the use case for static typing. I feel much more compelled to learn and use it on a smaller project now. Heads up typescript.

I wonder alot about where rust is at this moment in time. I saw a few things that might be indicators, but some were from years ago. Rust appears to be the most "loved" language in the stack overflow survey since '16. Rust was described as having a hard time targetting lots of different architecture reliably, though this was said 3 years ago.

Lastly, I am very grateful for the resources available and the time and energy generously given by the contributors. Thanks.

