---
id: DrawingSvg
title: Drawing SVG
author: Quddus George
author_title: Trying to string together an SVG workflow
tags: [digital painting, visual arts, web optimization]
---
Watching my freshly-four-year-old son get entirely lost in his drawing brings back alot of memories. Growing up I really enjoyed drawing. When I dove back into the art form as an adult I enjoyed access to the tremendous amount of information and education available freely on the web. 
<!--truncate -->
![Ridvan Logo](/img/rose.svg)

With SVG however I found it a bit more difficult to find workflows for creating web optimized artwork. So here are a few things that I have found helpful so far:

* SVG's drawn SVG first are much more efficient file size.
* Copy and pasting parts of your drawing to alter into another element saves alot more time dealing with vectors graphics.

## Inkscape

I found some instructional content explaining how to take a png or jpeg file and generate an svg with Inkscape. Here is an SVG weighing in at **437kb** I created that way.
![Violinist](/img/violin.svg)


Inkscape is an amazing open source project that merits a lengthy and thorough explaination and praise, however I am not well aqcuainted enough to do it justice.

## MacSVG

This is a great open source tool particularly for creating SVG animations. I havn't had the time to sit down and produce something beautiful with it yet (because I made my first attempt too complex 😆). 

## Vectornator

I got to spend a bit of time with a borrowed Ipad and enjoy Vecotrnator on a larger screen, than my usual phone. I enjoyed this tool alot. Many of its controls seemed to be intuitive and similar to Procreate. The resulting SVGs where much smaller than converted drawings, about 1/10th of the size. The rose above, and the art around this site were drawn on with this program.