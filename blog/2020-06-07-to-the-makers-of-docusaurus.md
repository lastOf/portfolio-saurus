---
id: toTheMakers
title: To the Makers of Docusaurus
author: Quddus George
author_title: with gratitude and a big smile
tags: [design, docusaurus, documentation]
image: /img/docu.svg
---

### To the folks who worked on the Docusaurus project,
![Working with Docusaurus](/img/docu.svg)
I love [working with it](../docs/doc1). Well done. The project lists some of its goals as *being easy to use*, *customizable*, and *easily maintained*. From my own perspective it does a great job with all three, and I readily recommend it to anyone looking to quickstart a static project.
<!-- truncate -->

#### Low barrier to entry

It took almost no time at all to jump in and start using Docusaurus. I used v2 because I was eager to see the latest additions to the project. I really how easy to use docusaurus is once I built a couple projects with Gatsby and Next.js templates. In each case the time and energy to learn ins and outs was nothing to shrug at. However with Docusaurus I really was left almost entirely focused on the content right from the start.

#### Excellent documentation
The documentation itself is really well organized, parseable, and extensive. It is clear that a good deal of thought was put into the user experience when laying out the documentation. No suprise here that the great product contributed to great documentation.

#### Powerful and decisive
Docusaurus felt opinionated at the right moments, leaving me with the features most important to sharing my code with the audience. This saved me alot of time and energy. It really let me focus on the content, and gave me the tools to try and make it engaging.

#### Clean and intuitive
The design is smooth, simple and responsive. Very polished, and not overcomplicated, nothing to get in the way of the content.

**You have my sincere gratitude and praise! Well done.**