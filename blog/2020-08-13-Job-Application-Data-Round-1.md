---
id: Jobsearch1
title: Job Application Data 1
author: Quddus George
author_title: The search for good work.
tags: [javascript, reflection, data]
image: https://portfoliosaurus.now.sh/img/jobSearch1Diagram.png
draft: true

---
Just rounded two months of searching for a Javascript Dev job, and I wanted to share some of the data that resulted. I applied to 95 jobs where:
* I felt I could definitely do the work.
* Javascript was at least the central technology.
* They did not ask for more than 3 years of experience.
* The listing was remote or in my home state.

Here is some of the distribution of the time I spent applying:

Jun 87h
Jul 32h
Aug 5h

I tracked time with [Superproductivity](https://super-productivity.com/).

<!-- truncate -->
![Sankey Diagram of the Search](/img/jobSearch1Diagram.png)

41 responded, 54 did not.

Array of response times from jobs that did respond (days between me reaching out and them responding.):
```js
[
   0,  0,  0,  1,  1,  1,  1,  1,  1,  1,
   1,  2,  2,  2,  2,  3,  3,  3,  3,  4,
   4,  5,  6,  6,  8, 11, 13, 20, 25, 25,
  25, 25, 25, 25, 25, 25, 25, 25, 25, 25 //One is missing, 
]
```
On average this is around 10 days.

I only started taking note of the pay listed towards the end, I don't have good data on that. The lowest was a part time job (offering 24k for 20 hours a week), and the highest were offering around 120k.

Array of Companies applied to:

```
[
  'Gitlab',
  'Basecamp',
  'Docusaurus',
  'Aragon One',
  'NewsOnChat',
  'Universal Biologic Machines',
  'Panoptyc',
  'Homepage',
  'EngagedMD',
  'Agrograph',
  'Tremendous',
  'CoWorkr',
  'AskWhai',
  'Hippo Education',
  'Agency Vista',
  'Civic Dinners',
  'CareValidate Inc.',
  'Knack',
  'Veliovgroup.com',
  'Camino',
  'Keenious',
  'Pack Systems',
  'Botmock',
  'Scientific Fly',
  'Webveloper',
  'Prismic',
  'Gitstart',
  'Formstack',
  'Infotrust',
  'Art&logic',
  'Tsp consulting',
  'HomeCEU',
  'MicroTech Global',
  'Vonage',
  'N/A',
  'P2puni',
  'Fast Enterprises',
  'Typeform',
  'Plectica',
  'Hotjar',
  'PowerToFly',
  'Squarespace',
  'Mozilla',
  'Wikimedia',
  'Khan Academy',
  'CompanyCam',
  'Imperfect foods',
  'Automattic',
  'Creative Mine',
  'Smile',
  'App Academy',
  'Ahara',
  'Instore',
  'Trash',
  'Rally',
  'Typehuman',
  'Learning tapestry',
  'Pivot interactives',
  'Procurenow',
  'Veed.io',
  'Dropbox',
  'Charidy',
  'Horry County',
  'Clemson',
  'EHouse',
  'American Luxury Coach',
  'Dropbox',
  'Wonder',
  'Base',
  'Kickoff',
  'Simplywise',
  'Homepage',
  'Fictive Kin',
  'Inside Sherpa',
  'Webveloper',
  'Flaneur',
  'Trash',
  'Paradi',
  'AskWhai',
  'LVRG',
  'FireHud',
  'Loyal',
  'CreditCards.com',
  'Lithios',
  'Carimus',
  'Verus',
  'Fema',
  'Protonmail',
  'Equips',
  'TheTaproom',
  'Vimeo',
  'Delphi digital',
  'Our Hometown',
  'Phonism',
  'Oddball'
]
```

The 95th and last job I applied to actually interviewed me. Noteworthy from that experience is:

* My resume didn't list experience with VMs, databases, or authentication, which they were looking for.
* Once I looked through each of the requirements I found I had worked with a comparable technology. For example Digital Ocean droplets vs AWS s3. So without looking further into it, these seemed like I didn't meet them, when I could easily pick up using them.
* They asked how much I was expecting as a salary, when I didn't answer in a number, but that I would be happy to talk further about it, they told me the job was paying around $110k. This experience matches advice I have seen.


I am excited to refine my data collection. I have decided to use two tables, below is the schema I am thinking I will use going forward:

```js
Applications = {
    ApplicationId: Number,
    Company: String,
    DateApplied: Date,
    JobTitle: String,
    Salary: Number, // Any ideas on the best way to capture a range such as 75k-95k
    Requirements: String, //Comma seperated list of hard reqs
    Preferred: String, //Comma seperated list of preferred qualifications
    Technologies: String, //Comma seperated list of technologies I have worked with at the time of applying
    ApplicationMethod: String, //Such as Oneclick, Website, Email, 
    FollowUp: Boolean, // Did I reach back out and follow up
    Referral: String, //referral name if any
    ResumeVersionUsed: String,
    CoverLetterVersion: String,
    ContactInfo: String, //Email if any
    CompaniesJobList: String, //URL of job listing, Could webscrap directly once I have a list possibly
    Importance: Number, //1-5 Scale based on how much id like the job
}

Screenings = {
    ScreeningId: Number,
    ScreeningType: String, //HR Interview, CodeTest, Pair Programming etc...
    Company: String,
    ContactName: String,
    ContactEmail: String,
    DateRequested: Date, // When did they request the screening
    DateCompleted: Date, // When did I complete the screening
    Questions: String, // Comma seperated
    Stories: String, // Preconfigured description of projects and technologies used shared in interview, comma seperated
    InfoTheyShared: String, //Any information about the company shared
    InfoIShared: String, //Any info I shared with the company.
}

Networking = {
    Name: String,
    Connection: String,
    Profession: String,
    DateContacted: Date,
    ReasonContacted: String,
    Response: String,
    TechIndustry: Boolean,
    
}

```