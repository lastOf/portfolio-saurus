---
id: preactsvelt
title: Preact and Svelte
author: Quddus George
author_title: Pensive Auditor
tags: [front-end frameworks, Reflection]
---

There are two front-end frameworks for building modern web applications which stand out to me the most. These are Preact and Svelte, and they are both appealing for the same reason, and yet different and almost polar in a way.

<!--truncate-->

Though they go about it in different ways, both Preact and Svelte lean in on the strengths of the language itself to succeed. I believe this is the most sustainable approach, and pays off quickly.

We can count on with confidence that the language and the browsers will both advance. The organizations, individuals, and corporations which are contributing are committed and invested in a powerful web, and a powerful javascript. As such, building the frameworks to work with the language instead of around it will, in principle, allow for Preact and Svelte to leverage those optimizations more easily and naturally.

Having not done much yet in either here are my some of my initial ideas, which hopefully pave the way for me to experiment with both of them.

## No Virtual DOM

In both cases there is no Virtual Dom, they use the concepts pioneered by React of only making the necessary DOM edits, but they do it in the DOM itself.

My impression is that this draws on the wonderful benefits of progressive web apps without divorcing from a central construct of the web.

## Htm and doing away with Jsx in Preact

[Htm](https://github.com/developit/htm) is a remarkable project from the creator of Preact Jason Miller. Htm uses the features of javascript itself to replace JSX!

From the project README:

> "The original goal for htm was to create a wrapper around Preact that felt natural for use untranspiled in the browser. I wanted to use Virtual DOM, but I wanted to eschew build tooling and use ES Modules directly."

I think this is great! Staying in the language itself, lets us better understand its own features and strengths, and how to better use them.

I haven't found much study material for htm, but I think I'll end up starting with a deeper dive into Tagged Templates themselves.

Combining both Htm and Preact stays under 3500 bytes!

Here is a [video](https://www.youtube.com/watch?v=LY6y3HbDVmg) where Jason Miller explains Preact and some of the concepts of progressive web apps.

## Leaning more heavily on the compiler

Svelte heads the other direction and is itself a smart compiler, taking your svelte code and composes efficient javascript. It looks and feels more like writing javascript in an html file, and it does a great job at allowing you to build components and their associated logic in as few lines as possible, while actually increasing readablity.

Rich Harris explains the thinking behind Svelte in this jsconf [video](https://www.youtube.com/watch?v=qqt6YxAZoOc).

## Conclusion, for now

I really cannot wait for a chance to try these technologies. I think both are a confident step in the right direction, and will be the tool of choice for my next projects.