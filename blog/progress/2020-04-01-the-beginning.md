---
id: beginning
title: The Beginning
author: Quddus George
author_title: in a retrospective moment
tags: [origin story, docusaurus]
---
![the first program](../static/img/firstProgram.png 'A simple html page that saved me hundreds of actions at a time.')

I remember the moment very clearly. I was studying through javascript, my first time coming into contact with data structures, functions, objects and arrays. In one of my journeys into stackoverflow I came accross the html mailto property.
<!--truncate-->
With some reflection and a surge of excitement I concluded that I could use the mailto property to make my job more efficient and save tons of time!

I worked as a digital web assistant for the Wilmette Institute, an online learning institution, doing digital paperwork. One of my main tasks was creating pdf certificates for learners and emailing them out one by one.

When I joined the team certificates were over a year behind, I had already been grouping windows, making groups of ten drafts one at a time before filling out the variable information.

This was a whole new level however, using the mailto property I could paste the course name, and all of the students who completed the course into some input fields and generate all the drafts at once. 