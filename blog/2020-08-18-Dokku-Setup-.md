---
id: Dokku
title: Setting up Dokku
author: Quddus George
author_title: The search for good work.
tags: [javascript, reflection, data]
image: https://portfoliosaurus.now.sh/img/jobSearch1Diagram.png
draft: true

---


1. Create a new VPS with digital ocean using the dokku image from the marketplace.
2. 


Deploy to dokku with git:

```
git remote add dokku dokku@<ip of vps>:<appname>
```
commit with git commit