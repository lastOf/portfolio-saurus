---
id: mail1
title: Give Me Back My Email
---

Give me back my email is my first useful open source project. It is a collection of Nodejs scripts that extract every email address in your gmail inbox and drop them in an array.

It is currently at the proof of concept stage.

Check it out in the [Gitlab Repository](https://gitlab.com/lastOf/give)
# But why though?
The inspiration for this project is twofold. On the one hand, one of the main obstacles that has prevented me from abandoning using gmail as my primary email address is how difficult it is to get contacts out. There is clearly a dataset under the surface, but the ui doesnt expose it, unless you manually load contacts into the gmail contact list. As far as I can tell, gmail is set up to avoid you doing this. I think this is a subtle technique to keep the user locked in.

Secondly, I wanted to gather all the contact information of friends and family old and new to tell them about an upcoming project. An album that explores the role of music itself!

# What did you find?

Around 3950 email addresses over more than 10 years of messages! Once I filtered out the support bots and automated email accounts it was around 3650 addresses. There were alot less unique automated email accounts than I expected.

It was so exciting looking over the data! I found people I used to do community service alongside, distant family members, childhood friends, old professors, and many friends who I have spent time with only breifly! Many of them I would have never thought of to share these projects with myself, and I am sure they will be excited by it.

Interestingly enough I also ended up with an email list almost as long as my workplace, a learning institution that has been serving internationally for some 25 years.

It also took a few hours.

# How does it work?

It uses googles Puppeteerjs library ironically. There is a nice library called Puppeteer Stealth that allows you to log into your gmail account without being detected as automation.

Then the Puppeteer goes through each page, opening every inbox email and grabbing the contacts.

Every once in a while gmail freezes your account for too many api calls. As far as I can tell this doesnt have any lasting effect and a few minutes later your account is accessible again. When this happens GMBME shuts down and drops a line in the terminal suggesting you try again in a few minutes. Your page number is saved, and used when you resume.

Finally, you can filter out the email addresses with another included script. This simply does a light filter removing addresses containing common strings such as 'support' or 'donot', which are common among automated emails, and a heavy filter leaving email addresses only from common email services such as gmail, yahoo, aol and so on.


# Command Line Arguments

GMBME lets you enter your username and password as command line arguments. In Nodejs command line arguments are exposed with ```process.argv```.