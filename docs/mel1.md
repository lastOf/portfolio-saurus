---
id: mel1
title: The Music
sidebar_label: The Music
---


:::info Ongoing Project
This project is ongoing, the writeup will unfold alongside the project itself. Check back in a month or so for an update. 
:::

The Most Melodious project is an exciting one! It includes a collection of 5 songs which explore the role of music today though the Writings of Bahá'u'lláh.

It is also an experiment in the realm of lending practical assistance to other young people striving to earn a living according to their skills and capacities.

## So what's the plan?

I purchased audio recording studio equipment for a friend who has a particular love for making music. Then after a few months commissioned the creation of this collection of music, of which I have the rights to.

My hope is that if I am able to apply what I have learned in the realm of web development to distribute the music successfully, it will clearly display the value of the artists work to the artist themselves. That the experience of creating the music as well as the success of it's sales will propel the artist further along their path and leave everyone with alot of valuable experience.