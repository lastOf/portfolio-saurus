---
id: house1
title: The Concept
sidebar_label: The Concept
---


[CSS House](https://lastofthefirst.github.io/) was my first portfolio site. It really reflects my love for using and understanding the built in capabilities of the languages themselves.

It was [written](https://github.com/Lastofthefirst/Lastofthefirst.github.io) in purely 'vanilla' css, html, and a dash of javascript for the animations.

While it was does not size up to the countless stunning codepens out there, early in my own journey as a programmer it was a great exploration of CSS that I truly enjoyed. I find the color pallate, as it appears on desktop Firefox very pleasant.

It is based on my own apartment, and houses links to some older projects.