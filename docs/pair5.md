---
id: pair5
title: Building for Production
sidebar_label: Building for Production
---






## A note on Building for Distribution

:::danger React and Electron and Webpack
This is where I hit the most snags. A number of steps need to be made to ensure that Webpack understands where to look for the renderer files and where to look for the main files. By default Electron expects the renderer to be plain hmtl, css and js files. I'll do my best to gather everything useful I found in this regard below.
:::


## Main Process

### Preload

//Access ipcRenderer through window.electron, which makes electron available globally through the preload js file.


### Electron Index.js

```js
const { app, BrowserWindow } = require('electron'); let win; function createWindow() {
    win = new BrowserWindow({
        width: 1000, height: 800, transparent: false,
        minWidth: 800, minHeight: 600,
        webPreferences: { // <--- (1) Additional preferences
            nodeIntegration: true,
            webSecurity: false,
            preload: __dirname + '/preload.js' // <--- (2) Preload script
        }
    });

    if (process.env.NODE_ENV === 'dev') {
        win.loadURL('http://localhost:3000')
        // win.webContents.openDevTools()
    } else {
        win.loadURL(`file:///${__dirname}/build/index.html`);
    }


    // <--- (3) Loading react

    //    win.webContents.openDevTools();
    win.on('closed', () => {
        win = null
    });
};

```

### Package.json Scripts

```
 "electron": "electron src",
    "start": "NODE_ENV=dev nodemon --watch ./src/**/* --watch . --exec 'yarn electron'",
    "dist": "rm -rf dist/; NODE_ENV=prod electron-builder -mw",
    "build:renderer": "cd ../renderer; yarn build; cd ../main; cp -r ../renderer/build ./src"```
```

### Extra Resources and Files


```json
  "extraResources": [
      "tmp/**/*",
      "data/*.db"
    ],
    "files": [
      "src/**/*",
      "build/**/*"
    ],
```

### icons
```json
    "win": {
      "target": "NSIS",
      "icon": "hearticon.ico"
    },
    "mac": {
      "icon": "hearticon.icns"
    }
```

## Renderer Process

### npm start

    "start": "BROWSER=none NODE_PATH=./src react-scripts start",

### 