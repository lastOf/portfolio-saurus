---
id: doc1
title: Docusaurus
sidebar_label: Docusaurus
---


[Docusuarus](https://v2.docusaurus.io/) is a static site generator primarily for building open source documentation websites. It's emphasis is on being easy to get up and running and easy to maintain. It was created and open sourced by a team at Facebook.

## Portfolio

For my portfolio, which I have titled *Portfoliosaurus* I was looking for a few things.

1. Get it up and running quickly.
1. Make my code accessible, especially for the projects which aren't just sitting in a public repo, like a desktop application or Puppeteer scripts specific to my employer.
1. Be beautiful, and organized.  

Docusaurus fit exactly what I was looking for! It's designed to display code, with features like editable codeblocks and live code blocks. Inherent to its design is a clear system for organizing documentation, which is written in markdown! Furthermore it has a simple, responsive, and clean design on any screen size.