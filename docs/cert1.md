---
id: cert1
title: Where it all began
sidebar_label: Where it all began
---

:::important This is it
The moment where I was first able to apply what I learned to a real problem, my first programatic solution.
:::
![the first program](/img/firstProgram.gif 'A simple html page that saved me hundreds of clicks at a time.')

When I [stumbled accross the mailto property](../blog/beginning) I realized it could greatly reduce my daily work. The [solution I built](https://github.com/Lastofthefirst/certSender) is a single html page that allowed me to create batches of templated email drafts. It made the process of copy-pasting learner names, email addresses, and course titles reduced, predictable and reliable.
```js
function sendMail() {
    // pull input information into an object
    // !! notice that each property of the object contains an array of values such as all of the addresses, or all of the learner names. 🤨
    let mailInfo = {
        address: document.getElementsByClassName('recipient'),
        learnerName: document.getElementsByClassName('learnerName'),
        course: document.getElementById('courseName').value,
    };
    //for each email input a mailto function is called with the addressee and email address of the same iterator
    for (let property in mailInfo.address) {
    if (mailInfo.address.hasOwnProperty(property)) {
        let email = mailInfo.address[property].value;
        let name = mailInfo.learnerName[property].value;
        let emailBody = `Dear ${name}:%0D%0A %0D%0A
We are pleased to send the attached Certificate of Completion for the Wilmette Institute course: ${mailInfo.course}. Thank you for completing the self-assessment survey telling us about your course experience. We are always eager to hear more about how the Wilmette Institute serves our participants, faculty and learners alike.
 %0D%0A %0D%0A`;
 let link = "mailto:" + email + '?cc=learn@wilmetteinstitute.org&subject=Wilmette Institute Certificate' + "&body=" + emailBody;
    ;
    window.location.href = link; 
    }
}
}
```
Here is the object that would be created in the example that I *gif* above:

```js
mailInfo = {
    address: ["independent@investigation.org", "ofTheTruth@duty.org"],
    learnerName: ["Indy Jones", "Truthy Boolson"]
}
```
Not exactly the standard way to use objects, but(!) this program reduced the number of clicks from some 200 down to around 40 per course!

Now, it's not beautiful html, javascript, or css for that matter, and it does a good job of defining the limitations of my understanding at the time.

```html
<body>

    <form>
            <br>
            <label>Number of Certificates:</label>
            <br>
<button onclick="addFields(1); return false">One</button>
<button onclick="addFields(2); return false">Two</button>
<button onclick="addFields(3); return false">Three</button>
<button onclick="addFields(4); return false">Four</button>
<button onclick="addFields(5); return false">Five</button>
<br>
<button onclick="addFields(6); return false">Six</button>
<button onclick="addFields(7); return false">Seven</button>
<button onclick="addFields(8); return false">Eight</button>
<button onclick="addFields(9); return false">Nine</button>
<button onclick="addFields(10); return false">Ten</button>
<br>
        <label> Course Name:
    <input type="text" id="courseName"></label>
    <div id="container"></div>
    </form> 



    <button id="sendButton" onclick="sendMail(); return false">Send</button>
</body>
```

:::tip Recognize!
This was, however, a great moment for me! From here on out I started asking *question after question*, which led to delving into Nodejs, Google's Puppeteerjs, and eventually **automating my entire job**.
:::


