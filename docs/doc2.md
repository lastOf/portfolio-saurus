---
id: doc2
title: Personalized
---

![A symbolic svg drawing. Planting a seed in good soil, a parallel for the trustworthiness and justice that must be the foundation for any noble endeavor.](/img/foundation.svg 'A symbolic svg drawing. Planting a seed in good soil, a parallel for the trustworthiness and justice that must be the foundation for any noble endeavor.')

Some of my own drawings, SVGs for the small file size, illustrate some of my own beliefs and views on work.

## Tell me a Story

Docusaurus' documentation organization really lends itself to telling a story. I enjoyed this alot. My own projects tell a story. Starting out with the simplest program I have ever written in a manner that reflects its profound role in my journey as a programmer.
