---
id: pair4
title: Business Logic
sidebar_label: Business Logic
---
import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Matching the Pairs

All of the functionality, the database, the select inputs, and everything in else, all surround matching the pairs from the events. 

So participants attend an event, spend a bit of time talking to one another, and list the people they liked. Now the event manager has to

The onchange function ended up being a bit longer than usual:


<Tabs
  defaultValue="compare"
  values={[
    { label: '1. Compare', value: 'compare', },
    { label: '2. if many deleted', value: 'many', },
    { label: '3. if one added', value: 'oneAdded', },
    { label: '4. if one deleted', value: 'oneDel', },
    { label: '5. Update', value: 'update', },
  ]
}>
<TabItem value="compare">

```jsx
    let changePicks = (params) => {
     // Params contains the current state of the select with the recent change
        let a = params || [];
            // Picks contains the previous state, the one currently in the database
        let b = picks || [];
            // We can compare the two to determine, was someone added to the list or removed
        let addedPicks = a.filter(el => !b.includes(el))
        let removedPicks = b.filter(el => !a.includes(el))
        let personIndex = session.participants.findIndex(el => el._id === person._id)
        session.participants[personIndex].picks = a || [];
        let participants = session.participants;
        let likesIds = person.picks.map(el => el.value)
```

</TabItem>
<TabItem value="many">

```jsx
 // If more than one person was removed at once the remove all button was clicked.
        if (removedPicks.length > 1) {
            person.matches = []; // remove all picks
            picks.map(el => { //remove this person from all other peoples matches.
                let them = session.participants.find(l => l._id.includes(el.value))
                them.matches = them.matches.filter(l => !l.value.includes(person._id))
            })
```

</TabItem>
<TabItem value="oneAdded">

```jsx
} else if (addedPicks.length > 0) {  // If someone was added
            let [theOtherPerson] = addedPicks;  // we find the index of the new addition

            let theOtherIndex = session.participants.findIndex(el => el._id === theOtherPerson.value)
            // check if the new additions picks contains ourself.
            let myValue = session.participants[theOtherIndex].picks.find(el => el.value.includes(person._id))
            // if they have picked us, and we havnt already matched with them.
            if (myValue && !person.matches.map(el => el.value).includes(theOtherPerson.value)) {
                // add one another as matches
                session.participants[theOtherIndex].matches.push(myValue);
                person.matches.push(theOtherPerson)
            }
```

</TabItem>
<TabItem value="oneDel">

```jsx
        } else if (removedPicks.length > 0) { // If one person was removed
            let [theOtherPerson] = removedPicks;
            let theOtherIndex = session.participants.findIndex(el => el._id === theOtherPerson.value)
            // Check if that person had me as a pick.
            let myValue = session.participants[theOtherIndex].picks.find(el => el.value.includes(person._id))
            if (myValue) { //if so remove me from their matches.
                session.participants[theOtherIndex].matches = session.participants[theOtherIndex].matches.filter(el => !el.value.includes(myValue.value));
                //and remove them from my own matches
                person.matches = person.matches.filter(el => !el.value.includes(theOtherPerson.value))
            }
        }
```

</TabItem>
<TabItem value="update">

```jsx

        let updated = session;
        // send the new value over to the db
        window.electron.ipcRenderer.send('addPicks', { updated })
        setPicks(params) // Aaaaand finally update the state for the select component
    }
```

</TabItem>
</Tabs>


:::danger Pack up the pencil and paper
Before PairUp the client used to determine all the matches by hand, and it reportedly took many weeks per event. Shortly before having PairUp built the client started using a [matrix](https://en.wikipedia.org/wiki/Matrix_(mathematics)) on paper similar to a [Punnet Square](https://en.wikipedia.org/wiki/Punnett_square), that alone saved them tons of time and helped reveal that there indeed was a better way.
:::

## Sortable Table

One of the pages shows a table containing the data for all the participants saved in the database thus far. Here we can edit any participants details and download a spreadsheet.

Since the downloadable csv would be sortable in any spreadsheet editor we really only needed to worry about sorting the data as it appears in-app. This means we can store the sort in state, and then map across the data after sorting it with state. So each button changes the state, sort by first name, last name, age, and so on. We can also add a Boolean state to reverse the sort when we click the button again, so we can sort z-a and a-z or 0-9 and 9-0. 

Now that I think of it, it would be really easy to add a filter For searching too. Be right back, I’m gonna go implement that.

<Tabs
  defaultValue="component"
  values={[
    { label: 'Component', value: 'component', },
    { label: 'State', value: 'state', },
    { label: 'Custom Toggle Hook', value: 'hook', },
  ]
}>
<TabItem value="component">

```jsx
<div className="tab-container row-container">
    <table>
        <thead>
            <tr>
<td><Button onClick={() => { setSort('first'); setReverse() }}>First Name</Button></td>
<td><Button onClick={() => { setSort('last'); setReverse() }}>Last Name</Button></td>
<td><Button onClick={() => { setSort('age'); setReverse() }}>Age</Button></td>
<td><Button onClick={() => { setSort('sex'); setReverse() }}>Sex</Button></td>
<td><Button onClick={() => { setSort('email'); setReverse() }}>Email</Button></td>
<td><Button onClick={() => { setSort('phone'); setReverse() }}>Phone</Button></td>
            </tr>
        </thead>
        <tbody>

            {reverseFunction([...list].sort(sortRows[sortType].fn)).map(person => {
                return <PeopleRow key={person._id} person={person} />
            })}
        </tbody>
    </table>
</div>
```
</TabItem>
<TabItem value="state">

```jsx
    let reverseFunction = (arr) => {
        if (reverse) {
            return arr.reverse()
        } else {
            return arr
        }
    }

    let [reverse, setReverse] = useToggle(true);
    let [sortType, setSort] = useState('default')
    let sortRows = {
        first: {
            class: 'first',
            fn: (a, b) => ('' + a.first.toUpperCase()).localeCompare(b.first.toUpperCase())
        },
        last: {
            class: 'last',
            fn: (a, b) => ('' + a.last.toUpperCase()).localeCompare(b.last.toUpperCase())
        },
        sex: {
            class: 'sex',
            fn: (a, b) => ('' + a.sex.toUpperCase()).localeCompare(b.sex.toUpperCase())
        },
        age: {
            class: 'age',
            fn: (a, b) => a.age - b.age
        },
        email: {
            class: 'email',
            fn: (a, b) => ('' + a.email.toUpperCase()).localeCompare(b.first.toUpperCase())
        },
        phone: {
            class: 'phone',
            fn: (a, b) => a.phone - b.phone
        },
        default: {
            class: 'default',
            fn: (a, b) => a
        }
    }

    let [list, setList] = useState(participants)
    useEffect(() => {
        setList(participants)
    }, [participants])
```

</TabItem>
<TabItem value="hook">

```jsx
function useToggle(initialState) {
    const [value, setValue] = useState(initialState);
    const toggle = () => { setValue(!value) };

    return [value, toggle];
};
```

</TabItem>
</Tabs>




## Don't Delete That!

When someone is deleted from the list of participants what happens to the people who liked them or matched with them.  If they remain on the lists it could lead to bugs or confusion from the user, especially if the list of participants is long. 

Tackling this was a bit harder than I anticipated because of a few different moving parts. 

For one, the state of each react-select was not accessible to the other rows.

In the end I just edited the entire event object,  pushed the new object to the database, and then  sent back the update, which is fine because we are using a desktop electron application, a server api call would be far too costly and time consuming.

```jsx

    let removeParticipant = () => {
        console.log(session.participants.map(el => el.matches.map(el => el.first)))
        let updated = session;
        let participation = session.participants;
        let updatedParticipants = participation.filter(el => !el._id.includes(person._id))
        let removedPick = updatedParticipants.map(el => {
            el.picks = el.picks.filter(l => !l.value.includes(person._id))
            el.matches = el.matches.filter(l => !l.value.includes(person._id))
            return el;
        })
        // console.log(removedPick)
        updated.participants = removedPick;
        window.electron.ipcRenderer.send('addPicks', { updated })
        setPicks(person.picks)
        forceUpdate();
        console.log(session.participants.map(el => el.matches.map(el => el.first)))
    }

```
