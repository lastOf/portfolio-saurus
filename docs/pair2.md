---
id: pair2
title: Embedded Database
sidebar_label: Embedded Database
---


1. React and Electron
    1. main and renderer processes
1. Embedded Database
1. Chakra ui
1. Neumorphism
1. Packaging
1. Client requested adjustment


## NEDB


For our embedded Database we will use Louis Chariot's [NEDB](https://github.com/louischatriot/nedb/), with a [wrapper for async calls](https://github.com/bajankristof/nedb-promises) created by Bajan Kristof. This way our data is persistent, it's still there when we open the program back up, and scales well.

:::information It's nice when it just fits 
NEBD was a great option. It's lightweight, fast, it has the same api as MongoDB (its great to use what you know), and I even found some useful materials giving [specific examples](https://medium.com/@shivekkhurana/persist-data-in-electron-apps-using-nedb-5fa35500149a) on how to set it up!
:::

We set up NEDB in our *main process*, and the setup is quite simple.
```
$ yarn add nedb-promises
```

First we set up a db factory.
```js
const {app} = require('electron');
const Datastore = require('nedb-promises');

//We can call this function to create a new database.
const dbFactory = (fileName) => Datastore.create({
  filename: (process.env.NODE_ENV === 'dev' ? `${__dirname}/..` : process.resourcesPath) + `/data/${fileName}`, 
  timestampData: true,
  autoload: true
});
// In my case I needed two datasets, one for the participants and one for the events themselves.
const db = {
    participants: dbFactory('participants.db'),
    sessions: dbFactory('sessions.db')
  };
  // Now we can export so that we can access our database from scripts in our main process.
  module.exports = db;
```
We can find our two databases in the neighboring data folder:

```
main process
├── data
│   └── participants.db
│   └── sessions.db
├── db.js
├── ...
```

Now we can access our databases from anywhere in the main process:

```js
let db = require('./db.js')

ipcMain.on('make-person', async (event, args) => {
    let person = args;

    let createParticant = async (obj) => {
        let participant = await db.participants.insert(obj)
        return participant;
    }
    createParticant(person)
    let participants = await db.participants.find({}); //find all participants
    event.reply('return-participants-list', participants)
});

```






