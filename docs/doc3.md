---
id: doc3
title: Conclusion
---

Docusaurus was an excellent choice. I am enjoying it so much, I can hardly imagine having used a different approach. In closing, here are a few more benefits that with Docusaurus.

## Top Notch

![Mobile lighthouse report.](/img/mobileReport.png 'Portfoliosaurus gets a great lighthouse report right out of the box.')

An advantage that should not be overlooked, Docusaurus projects score quite excellent lighthouse reports right out of the box. Just look at the mobile scores from this site!

Performance, accessability, best practices, and search engine optimization, all crucial elements for web projects all built in. This freed me up to focus on the content.

## Docusaurus' Documentation

This was what initially got me interested in using the project for my portfolio. Docusaurus itself has really well organized, clear, robust documentation. It's a good sign that the Docusarus project utelizes its own project to help others to leverage it.

## Markdown

Being able to have a blog, and documentation written in markdown saved a considerable amount of time. The use of remark-admonitions added some nice flexibility and gave life to narrating the projects. MDX support allows us to use react in markdown, at that point are there any drawbacks? It's faster to type and format by default, but has the power of React when necessary.

